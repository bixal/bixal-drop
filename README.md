# Bixal Drop, A Drupal 8 & 9 base theme

## Table of contents

- [Getting started](#markdown-header-getting-started)
- [Theming and component methodology](#markdown-header-theming-and-component-methodology)
- [Component theming example walk-through](#markdown-header-component-theming-example-walk-through)
- [Local development environment](#markdown-header-local-development-environment)
- [PHP](#markdown-header-php)
- [Xdebug](#markdown-header-xdebug)
- [Sass / SCSS](#markdown-header-sass-scss)
  - [Gulp Sass workflow](#markdown-header-gulp-sass-workflow)
  - [Media Queries and Breakpoints](#markdown-header-media-queries-and-breakpoints)
  - [Sass Linting](#markdown-header-sass-linting)
  - [CSS formatting](#markdown-header-css-formatting)
  - [BrowserSync and live CSS injection](#markdown-header-browsersync-and-live-css-injection)
  - [Units](#markdown-header-units)
  - [Sass Mixins](#markdown-header-sass-mixins)
- [Grids](#markdown-header-grids)
- [Drupal responsive image breakpoints](#markdown-header-drupal-responsive-image-breakpoints)
- [SVG icons](#markdown-header-svg-icons)
- [Javascript](#markdown-header-javascript)
- [Theming helpers and best practices](#markdown-header-theming-helpers-and-best-practices)
  - [Twig Field Value](#markdown-header-twig-field-value)
  - [Twig Tweak](#markdown-header-twig-tweak)
  - [Link attributes](#markdown-header-link-attributes)
  - [Twig template debug output](#markdown-header-twig-template-debug-output)
  - [Xdebug within Twig templates](#markdown-header-xdebug-within-twig-templates)
  - [Coding tips and best practices](#markdown-header-coding-tips-and-best-practices)
- [Road map](#markdown-header-road-map)

![Bixal Drop screenshot](screenshot.png "Bixal Drop screenshot")

## Getting started
There are a few ways you can obtain this theme.

1. You can download a Zip file from this repository.
2. Clone with Git `git clone git@bitbucket.org:bixal/bixal-drop.git` (make sure to remove the .git folder so you can commit Bixal Drop to your main repository.)

Now that you have the theme in your local setup, you should enable it.

## Theming and component methodology
This theme does not use a base theme such as Classy or Stable, it serves as its own base theme. A few features from Classy such as some of the base templates for node, taxonomy term, field, and block have been added. It might be good to think of this theme as a scaffolding or starter point rather than a polished finished product.

The main idea behind this theme is to create component based CSS in order to support the best site performance possible. This means individual CSS files on a per component basis as well as a global file that includes CSS needed site-wide. This follows methodology being used in Drupal core as well such as in `/core/themes/claro/claro.libraries.yml` which can serve as a good guide on how to do this.

## Component theming example walk-through

As an example, let's say you have a Hero component using the Paragraphs module. The hero might have these fields:

* Media / image
* Title / heading
* Description / text

When this renders on the page, you might see a Twig template suggestion as:

```bash
paragraph--hero--default.html.twig
```

In this template, you can render a custom library just for the hero.

```twig
{{ attach_library('bixal_drop/hero') }}
```

Then in `bixal_drop.libraries.yml` file, you would add the call to this library.

```yaml
hero:
  css:
    theme:
      build/dist/css/components/hero.css: { }
```

Note the path `build/dist/css/components/hero.css`. You'd then create a corresponding SCSS file located in `build/src/scss/components/hero.scss`.

Back in the hero template, you can set a top level class by using:

```twig
{% set classes = [
  'hero',
  'hero--' ~ modifier | clean_class,
  'component__item',
  'component__item--type--' ~ paragraph.bundle|clean_class,
  view_mode ? 'component__item--view-mode--' ~ view_mode|clean_class,
  not paragraph.isPublished() ? 'component__item--unpublished'
] %}
```

Note the presence of `hero` as the top level component class as well as a modifier, `'hero--' ~ modifier | clean_class,` which could be used for an alternate style for example. The modifier variable might come from a boolean or select list from the component.

Then, you can code markup suited for a component based CSS workflow using [Twig Field Value](https://www.drupal.org/project/twig_field_value).

```twig
{% block paragraph %}
  <div{{ attributes.addClass(classes) }}>
    {% block content %}

      <div class="hero__wrap">
        <div class="hero__wrap-inner">

          <div class="hero__media">
            <div class="hero__background-image">
              {{ content.field_background | field_value }}
            </div>
          </div>

          <div class="hero__title">
            <div class="hero__title-wrap">
              <div class="hero__title-inner">
                <h1>{{ content.field_heading | field_value }}</h1>

                {% if content.field_text | render %}
                  <div class="hero__text">
                    {{ content.field_text | field_value }}
                  </div>
                {% endif %}

              </div>
            </div>
          </div>

        </div>
      </div>
    {% endblock %}
  </div>
{% endblock paragraph %}
```

For instances of multiple *paragraph items* within a component, you will most likely have two templates one for the main wrapper and one for the items within and you can theme using the same root class across template files.

## Local development environment
This theme has been designed with [Lando](https://docs.lando.dev/) in mind. Lando is a local development server which can help streamline tasks such as using Composer, Node, Xdebug, PHPCS, and more. The idea is that you do not have to install all these add-ons separately on your local machine. Rather, they are installed for you right within Lando. The benefit is that all these add-ons get installed for you via a configuration file in Lando. See [https://docs.lando.dev/config/recipes.html#usage](https://docs.lando.dev/config/recipes.html#usage) for more information.

This theme has an example Lando setup located in `./example-settings/example.lando`. See the README file in that directory for more information.

## PHP
There are PHP include files located at `./build/src/main/php-includes`. These files will come in handy for preprocessing, theme hook suggestions, and form alters. If you'd like to add additional files, you may add them to [src/main/bixal_drop.theme](src/main/bixal_drop.theme). The include files have some basic starter code, specifically to add block region names to blocks and menus. This comes in handy for advanced theming.

You should also be running PHPCS on the theme directory as your developing to ensure adhering to PHP and Drupal coding standards.

## Xdebug
Xdebug can really help you with theming both in PHP and Twig contexts. The example Lando files will help you get up and running with Xdebug, see the README file in 	`./example-settings/example.lando`.

## Sass / SCSS
For this project, we are taking a granular approach via individual CSS files being output and then compartmentalized into various libraries loaded from
specific templates. This aligns with Drupal core but gives us the ability to boost performance as well.

### Gulp Sass workflow

To get up and running with Gulp, `cd` into the theme root located at `web/themes/custom/bixal_drop/` and run `lando npm install`.

There are a few Gulp tasks setup for this theme.

1. `lando gulp` - Default task - This runs the default task.
2. `lando gulp svg` - This compiles the SVG sprite. There are some sample icons in the `./build/src/icon/svg` folder, replace these as needed. Note, there are also mixins for SVGs as well, see the mixins section below.
3. `lando gulp minify` - This will minify CSS and JS. It's up to the specific site developers on how to utilize the minified files, most likely in a pipeline or automated build on a remote server. Otherwise, something like Advanced Aggregation could be considered using the existing un-minified files.
4. `lando gulp combine` - This will concatenate anything in global to then be used in CKEditor.

**@todo** - add Lando scripting for more automation such as npm install and cd'ing into the theme directory.

### Media Queries and Breakpoints
We are using Sass MQ for media queries as it's very versatile. They have great documentation here: [https://github.com/sass-mq/sass-mq](https://github.com/sass-mq/sass-mq)

The predefined breakpoints are below, adjust as needed for your theme and requirements.
```scss
zero: 0,
narrow: 375px,
narrow_plus: 520px,
medium: 768px,
medium_plus: 920px,
large: 1024px,
large_plus: 1140px,
wide: 1280px,
wide_plus: 1440px,
mega_wide: 1600px,
```

#### Examples
##### Min width
```sass
@include mq($from: large) {
```

##### Range
```sass
@include mq($from: large, $until: wide) {
```

##### Max-width
```sass
@include mq($until: large) {
```

Of course you can always write your own media query from scratch if none of the above predefined breakpoints fit to your specific theming use case.

**Note**, write media queries in small chunks within individual selectors rather than encompassing many selectors with one giant media query. This makes it easier when another developer comes along some day and is making alterations to your code.

Good example, easier to maintain, media query within each selector:

```scss
   .header-no-lede {
      @include mq($from: large) {
       // CSS here.
      }
    }

    .header-has-link {
      @include mq($from: large) {
       // CSS here.
    }
```

Bad example, hard to maintain:

```scss
@include mq($from: large) {

  .foo {
   // CSS here.
  }

  .bar {
   // CSS here.
  }

  .header-no-lede {
   // CSS here.
   }

   .header-has-link {
    // CSS here.
   }
}
```

### Sass Linting
This project uses Sass linting (`gulp-sass-lint`) to help you write better CSS. When you run `lando gulp`, you will see a list of errors and warnings print out in terminal if there are any issues. An error will stop gulp so you can fix it and a warning will still continue to compile. If you need to adjust the linting settings, you can look in `./sass-lint.yml`.

### CSS formatting
This project uses CSS Comb (`gulp-csscomb`) to nicely format your CSS to adhere to standards from Drupal Coder.

### BrowserSync and live CSS injection
The gulpfile is set to reload pages upon save using BrowserSync and refresh CSS while theming using `lando gulp`. As you make changes to Sass, your CSS will get injected *without a page reload* for a more seamless and rapid theming process.

### Units
* For font sizes use the REM mixin. e.g. `@include rem(font-size, 48px);` which compiles as `font-size: 3rem;`.
* You can also use the REM mixin for other elements such as margins and letter spacing for example. See `./build/src/scss/global/typography.scss` for examples.

### Sass Mixins
There are various mixins files in the theme.

```bash
_mixins.scss
_mixins-grid.scss
_mixins-icon.scss
_mixins-typography.scss
```

## Grids
Bixal Drop takes a very lightweight approach to grids leveraging mixins in `_mixins-grid.scss`. See [https://github.com/judus/grid-scss](https://github.com/judus/grid-scss) for documentation. Of course you are free to write your own code for grids. The idea behind this is to not impose a large framework like Bootstrap or Foundation.

## Drupal responsive image breakpoints
The file, `bixal_drop.breakpoints.yml` contains code to get started using responsive images in the Drupal UI.

## SVG icons
* There are three mixins to utilize SVG icons that take arguments, some of them optional.

* `@mixin icon-before()`
* `@mixin icon-after()`
* `@mixin icon()`

These take various arguments, the `null` ones being optional:

```scss
$icon,
$width,
$height,
$rotate: null,
$position: null,
$top: null,
$right: null,
$bottom: null,
$left: null,
$opacity: null,
$background-position: null,
$translate_y: null,
$margin: null,
$padding: null,
$z_index: null,
```

```scss
@include icon-before(icon-letters, 35px, 35px, $position: absolute);
```

Noting that if you use `icon-before` or `icon-after` with position absolute, you may need to set a relative position to the containing element.

## Javascript
There is a global JS file located at `./build/src/js/bd-global.js`. This is for global ES6 code. Similar to the way the CSS will be componentized via libraries, the same should hold true with any JS files which can be added into the `./build/src/js` directory.

Bixal Drop also uses the new [Vanilla Drupal Once Library](https://www.drupal.org/project/once) which will eventually be in Drupal core and replace jQuery Once. There are a few examples in `./build/src/js/bd-once-samples.js`.

## Theming helpers and best practices

### Twig Field Value

[Twig field value](https://www.drupal.org/project/twig_field_value) allows you to get data from field render arrays. It gives you more control over the output without drilling deep into the render array or using preprocess functions.

### Twig Tweak

Twig Tweak is a handy module for rendering entities directly in a template.

For documentation, see [the twig tweak cheat sheet](https://www.drupal.org/docs/8/modules/twig-tweak/cheat-sheet) and ["Rendering blocks with Twig Tweak"](https://www.drupal.org/node/2964457).

To discover plugin ids, you can run this command:

```bash
lando drush ev "print_r(array_keys(\Drupal::service('plugin.manager.block')->getDefinitions()));"
```

Once you discover your plugin id, you can render it right in the template like this:

```twig
{{ drupal_block('simple_gse_search_block') }}'
```

### Link attributes

[Link attributes](https://www.drupal.org/project/link_attributes) lets you add custom attributes to menu items in the Drupal menu UI, the best use case being custom classes.

### Twig template debug output

To figure out what template is generating the markup for a particular element, you can use Twig's built in debug option. This option will render HTML comments alongside your rendered output that include the theme hooks in use, suggested template filenames, as well as denoting the exact Twig file used to render a particular section of your markup.

To enable Twig template output, you should have local settings files in `/sites/default`

* `settings.local.php`
* `local.services.yml`

There are examples of these in the theme within the `example.settings` folder in the root of the theme. You can move these to `/sites/default` and then just ensure that neither file gets committed to your repository.

### Xdebug within Twig templates

You can use Xdebug within Twig templates via the Devel module. Add `{{ drupal_breakpoint() }}` to your template and start debugging. You'll need the devel module enabled and Xdebug turned on in Lando.

### Coding tips and best practices
* If possible, use [BEM style CSS syntax](https://css-tricks.com/bem-101/) in combination with Twig Field Value. For example:

```twig
    <div class = "column">
      {% if content.field_iconcard_title | render %}
        <div class="column__card-title">
          {{ content.field_iconcard_title | field_value }}
        </div>
      {% endif %}
    </div>
```

* Use [PHP Code Sniffer (PHPCS) / Drupal coder](https://www.drupal.org/docs/8/modules/code-review-module/installing-coder-sniffer) for adhering to Drupal coding standards
* Use [Xdebug](https://xdebug.org/)
* Generously comment your code.

## Road map
* Upgrade to Gulp 4
* Add examples
