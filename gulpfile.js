'use strict';

const gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  rename = require('gulp-rename'),
  svgSprite = require('gulp-svg-sprite'),
  concat = require('gulp-concat'),
  sassLint = require('gulp-sass-lint'),
  cssComb = require('gulp-csscomb'),
  minify = require('gulp-uglify');

// Add browsersync.
gulp.task('browser-sync', ['sass'], function () {
  browserSync.init({
    injectChanges: true,
    logPrefix: 'Default Theme',
    baseDir: './',
    open: false,
    notify: true,
    proxy: 'd9sandbox.lndo.site',
    host: 'd9sandbox.lndo.site',
    openBrowserAtStart: false,
    reloadOnRestart: true,
    port: 31953,
    ui: false,
  });
});

// SVG icons.
gulp.task('svgSprite', function (done) {
  // Basic configuration example.
  var config = {
    shape: {
      dimension: {
        maxWidth: 80,
        maxHeight: 80
      },
      spacing: {
        padding: 8
      },
    },
    mode: {
      view: {
        bust: false,
        common: 'ico',
        example: {
          dest: '../build/dist/icon/icons.html'
        },
        prefix: '.',
        render: {
          scss: {
            template: './build/src/icon/svg-sprite-template.txt',
            dest: '../build/src/scss/_icons.scss'
          }
        },
        sprite: '../build/dist/icon/icons.svg',
      }
    }
  };

  gulp.src('**/*.svg', {cwd: './build/src/icon/svg'})
    .pipe(svgSprite(config))
    .pipe(gulp.dest('./'));
  done();
});

// JS.
gulp.task('js', function () {
  return gulp.src(['!./build/src/js/utils/**/*.js', './build/src/js/**/*.js'])
    .pipe(gulp.dest('./build/dist/js'))
    .on('error', function (err) {
      console.log(err)
    })
});

// Main Sass / CSS compile.
gulp.task('sass', function () {
  return gulp.src(['./build/src/scss/**/*.scss'])
    .pipe(sassLint({
      configFile: './sass-lint.yml',
    }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
    .pipe(sourcemaps.init())
    .pipe(sass({
      // Include node_modules paths as needed.
      includePaths: ["./node_modules/sass-mq/"],
      outputStyle: 'expanded',
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 4 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(cssComb('./csscomb.json'))
    .pipe(gulp.dest('./build/dist/css'))
    .pipe(browserSync.reload({
      stream: true,
      match: '**/*.css'
    }));
});

// Combine the Global css files into one for ckeditor to use.
gulp.task('concat', function () {
  return gulp.src('./build/dist/css/global/*.css')
    .pipe(cssComb('./csscomb.json'))
    .pipe(concat('ckeditor.css'))
    .pipe(gulp.dest('./build/dist/css'));
});

// JS minify for deploy
gulp.task('jsMin', function () {
  return gulp.src('./build/src/js/**/*.js')
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(minify())
    .on('error', function (err) {
      console.log(err)
    })
    .pipe(gulp.dest('./build/dist/js/min'))
});

gulp.task('cssMin', function () {
  return gulp.src(['./build/dist/css/**/*.css'])
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sass({
      // Include node_modules paths as needed.
      includePaths: ["./node_modules/sass-mq/"],
      outputStyle: 'compressed',
    }).on('error', sass.logError))
    .pipe(gulp.dest('./build/dist/css/min'))
});

// browser-sync watch.
gulp.task('watch', ['browser-sync'], function (gulpCallback) {
  gulp.watch("./build/src/scss/**/*.scss", ['sass']);
  gulp.watch("./build/src/js/**/*.js", ['js']).on('change', browserSync.reload);
  gulp.watch("./templates/**/*.html.twig").on('change', browserSync.reload);
  // Notify gulp that this task is done.
  gulpCallback();
});

// Task: Default gulp build and watch for CSS and JS.
gulp.task('default', ['sass', 'js', 'watch']);
// Task: handle SVGs.
gulp.task('svg', ['svgSprite']);
// Concat CSS for ckeditor.
gulp.task('combine', ['concat']);
// Task: Deploy assets.
gulp.task('minify', ['cssMin', 'jsMin']);
