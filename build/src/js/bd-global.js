/**
 * File JS functions.
 * NOTE*** This file is for vanilla JS. If you need jQuery, add $ in the function call.
 * @file
 * once vanilla - see https://www.drupal.org/project/drupal/issues/2402103.
 */
(function (Drupal, drupalSettings, once) {
  /*
  * File functions.
  */
  Drupal.behaviors.bixaldropA11y = {
    attach: function (context, settings) {
      // Detect the "I am a keyboard user" key.
      // Check if the user is using keyboard navigation (tabbing) and if so, add a class.
      function handleFirstTab(event) {
        if (event.key === 'Tab') {
          document.body.classList.add("user-is-tabbing");
          window.removeEventListener("keydown", handleFirstTab);
        }
      }

      window.addEventListener("keydown", handleFirstTab);
    },
  };

})(Drupal, drupalSettings, once);
