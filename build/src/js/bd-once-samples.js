/**
 * File JS functions.
 * NOTE*** This file is for vanilla JS. If you need jQuery, add $ in the function call.
 * @file
 * once vanilla - see https://www.drupal.org/project/drupal/issues/2402103.
 */
(function (Drupal, drupalSettings, once) {
  /*
  * File functions.
  */
  Drupal.behaviors.bixaldropOnceSamples = {
    attach: function (context, settings) {
      // Once for querySelectorAll.
      const foo_file_name = once('foo-file-name-once', context.querySelectorAll('.foo--file-name'))
      // Once for getElementById.
      const foo_custom_message = once('foo-message-once', context.getElementById("foo-glossary") || [])
    },
  };

})(Drupal, drupalSettings, once);
