<?php

/**
 * @file
 * Functions to support theming in the bixal_drop theme.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Allow themes to alter the theme-specific settings form.
 */
function bixal_drop_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['theme']['custom'] = [
    '#type' => 'fieldset',
    '#title' => 'Custom Settings',
  ];

  // Prefetch google fonts.
  $form['theme']['custom']['google_fonts_prefetch'] = [
    '#type' => 'checkbox',
    '#title' => 'Prefetch Google fonts',
    '#description' => 'Check this box if you will be using google fonts. This setting will enhance performance',
    '#default_value' => theme_get_setting('google_fonts_prefetch', 'bixal_drop'),
  ];
}
