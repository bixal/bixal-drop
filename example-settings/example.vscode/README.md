# VS Code and Xdebug 3

If you use VS Code and want to use Xdebug, put `launch.json` in the root of your site within `/.vscode/`
This will help get Xdebug running inside VS Code. See https://docs.lando.dev/guides/lando-with-vscode.html#getting-started
for more info.
