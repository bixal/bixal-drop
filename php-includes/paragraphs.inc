<?php

/**
 * @file
 * Functions to support theming in the Bixal Drop theme.
 */

/**
 * Prepares variables for paragraph templates.
 *
 * Default template: paragraph.html.twig.
 *
 * Most themes use their own copy of paragraph.html.twig. The default is located
 * inside "templates/paragraph.html.twig". Look in there for the
 * full list of variables.
 *
 * @param array $vars
 *   An associative array containing:
 *   - elements: An array of elements to display in view mode.
 *   - paragraph: The paragraph object.
 *   - view_mode: View mode; e.g., 'full', 'teaser'...
 */
function bixal_drop_preprocess_paragraph(array &$vars) {
}

/**
 * Implements hook_theme_suggestions_paragraph_alter().
 */
function bixal_drop_theme_suggestions_paragraph_alter(array &$suggestions, array $vars) {
}
