<?php

/**
 * @file
 * Functions to support Node theming in the Bixal Drop theme.
 */

use Drupal\node\NodeInterface;

/**
 * Implements hook_preprocess_html().
 */
function bixal_drop_preprocess_node(&$vars) {
  // Define the node.
  $node = \Drupal::routeMatch()->getParameter('node');
  if ($node instanceof NodeInterface) {
    // In case you need to be within a node context.
  }
}

/**
 * Implements hook_theme_suggestions_node_alter().
 */
function bixal_drop_theme_suggestions_node_alter(array &$suggestions, array $vars) {
}
