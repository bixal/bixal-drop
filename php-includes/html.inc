<?php

/**
 * @file
 * Functions to support HTML theming in the Bixal Drop theme.
 */

use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Implements hook_preprocess_html().
 */
function bixal_drop_preprocess_html(&$vars) {
  // Define the route match.
  $route_match = \Drupal::routeMatch();
  // Define the node.
  $node = \Drupal::routeMatch()->getParameter('node');
  // For lengthy includes.
  require_once dirname(__FILE__) . '/preconnect_html.inc';

  // Other functions can go below.
  if ($node instanceof NodeInterface) {
    // Define the node id.
    $nid = $node->id();
    // Add a class to identify the node id.
    $vars['attributes']['class'][] = 'nid-' . $nid;
  }

  // If taxonomy term page.
  // Add helpful classes to identify the
  // term id and what Vocab it belongs to.
  if ($route_match->getRouteName() == 'entity.taxonomy_term.canonical' &&
    ($term = $route_match->getParameter('taxonomy_term'))
    && $term instanceof TermInterface) {
    $vars['attributes']['class'][] = 'path-taxonomy--' . strtolower($term->get('vid')->getString());
    $vars['attributes']['class'][] = 'path-taxonomy-term-' . strtolower($term->get('tid')->getString());
  }
}
