<?php

/**
 * @file
 * Includes for various html functions. This is an include from html.inc.
 */

// Prefetch google fonts.
// Get the theme setting.
$google_fonts_prefetch = theme_get_setting('google_fonts_prefetch');
// If the setting is true, than add the prefetch code.
if ($google_fonts_prefetch === 1) {
  $head_links_google_api = [
    '#tag' => 'link',
    '#attributes' => [
      'rel' => 'preconnect',
      'href' => '//fonts.googleapis.com',
    ],
  ];

  $vars['page']['#attached']['html_head'][] = [
    $head_links_google_api,
    'head_links_google_api',
  ];

  $head_links_google_gstatic = [
    '#tag' => 'link',
    '#attributes' => [
      'rel' => 'preconnect',
      'href' => '//fonts.gstatic.com',
      'crossorigin' => '',
    ],
  ];

  $vars['page']['#attached']['html_head'][] = [
    $head_links_google_gstatic,
    'head_links_google_gstatic',
  ];
}
