<?php

/**
 * @file
 * Functions to support Block theming in the Bixal Drop theme.
 */

use Drupal\block\Entity\Block;

/**
 * Implements hook_preprocess_HOOK().
 */
function bixal_drop_preprocess_block(&$vars) {
  // Load the region so we can leverage it elsewhere.
  if (isset($vars['elements']['#id'])) {
    $block = Block::load($vars['elements']['#id']);
    $region = $block->getRegion();
    $vars['content']['#attributes']['region'] = $region;
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function bixal_drop_theme_suggestions_block_alter(array &$suggestions, array $vars) {
  // Provide suggestion for block templates by custom block type.
  if (!empty($vars['elements']['content']['#block_content'])) {
    $block = $vars['elements']['content']['#block_content'];
    // Add `block--BLOCK-TYPE.html.twig`.
    $suggestions[] = 'block__' . $block->bundle();
    $view_mode = $vars['elements']['#configuration']['view_mode'];
    if (!empty($view_mode)) {
      // Add `block--BLOCK-TYPE--VIEW-MODE.html.twig`.
      $suggestions[] = 'block__' . $block->bundle() . '__' . $view_mode;
    }
  }

  // Add theme suggestions to blocks based on region.
  if (!empty($vars['elements']['#id'])) {
    $block = Block::load($vars['elements']['#id']);
    $suggestions[] = 'block__' . $block->getRegion();
    $suggestions[] = 'block__' . $block->getRegion() . '__' . $vars['elements']['#id'];
  }
  return $suggestions;
}
