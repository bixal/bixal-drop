<?php

/**
 * @file
 * Menu functions to support theming in the Bixal Drop theme.
 */

/**
 * Implements hook_preprocess_menu().
 */
function bixal_drop_preprocess_menu(&$vars) {
}

/**
 * Implements hook_theme_suggestions_menu_alter().
 *
 * Provide region based menu suggestions.
 */
function bixal_drop_theme_suggestions_menu_alter(&$suggestions, array $vars) {
  // Add theme suggestions to menus based on block region.
  if (isset($vars['attributes']['region'])) {
    $suggestion = 'menu__' . $vars['menu_name'] . '__region_' . $vars['attributes']['region'];
    $suggestion = str_replace('-', '_', $suggestion);
    $suggestions[] = $suggestion;
  }
}
