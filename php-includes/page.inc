<?php

/**
 * @file
 * Functions to support Page theming in the Bixal Drop theme.
 */

/**
 * Implements hook_preprocess_HOOK() for page.html.twig.
 */
function bixal_drop_preprocess_page(array &$vars) {
}

/**
 * Implements hook_page_attachments().
 */
function bixal_drop_page_attachments_alter(array &$attachments) {
}
