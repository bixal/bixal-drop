<?php

/**
 * @file
 * Form functions to support theming in the Bixal Drop theme.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function bixal_drop_form_alter(&$form, FormStateInterface $form_state, $form_id) {
}
